# Jorels Odoo Addons

Jorels Odoo Addons

## Name
Jorels Odoo Addons

## Description
Modulos de Odoo de codigo y licencia abierta.

## Visuals
Para un demo visite: https://demo.jorels.com

## Installation
Instalar normalmente como cualquier otro modulo de Odoo.

## Usage
Usar como cualquier otro modulo de Odoo, teniendo en cuenta la reglamentación legal que corresponda en las configuraciones de Odoo.

## Support
Email: info@jorels.com

Telegram: @Jorels_SAS

## Roadmap
Versión 16 de Odoo

## Contributing
Colaboradores varios citados en cada proyecto.

## Authors and acknowledgment
Jorels SAS

## License
LGPL v3 y AGPL v3, dependiendo el modulo.

## Project status
Actualmente los modulos funcionan perfectamente en Odoo 12, 13, 14 y 15.
